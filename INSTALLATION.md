# Installation

If you want to use this for running your own information page, it is pretty simple to install. For Debian-based Linux distributions, do the following:

```
# Update the system
## It is generally a good practice to do this when administering a Linux server.
sudo apt update
sudo apt full-upgrade

# Install required packages
## First, install some of the packages
sudo apt install git build-essential postgresql postgresql-contrib cmake nodejs npm

## Then, download and add the Erlang repository.
wget -P /tmp/ https://packages.erlang-solutions.com/erlang-solutions_2.0_all.deb
sudo dpkg -i /tmp/erlang-solutions_2.0_all.deb

## After that, install Erlang and Elixir
sudo apt update
sudo apt install elixir erlang-dev erlang-nox

# Install Fuck Gab!
## First, add a new system user for the Fuck Gab! service.
sudo useradd -r -s /bin/false -m -d /var/lib/fuck-gab -U fuck-gab

## Secondly, git clone the Fuck Gab! repository and make the Fuck Gab! user the owner of the directory.
sudo mkdir -p /opt/fuck-gab
sudo chown -R fuck-gab:fuck-gab /opt/fuck-gab
sudo -Hu fuck-gab git clone https://code.sandiamesa.com/traboone/fuck-gab /opt/fuck-gab

## Then, change to the new directory:
cd /opt/fuck-gab

## Next, install Fuck Gab's dependencies and answer with yes if it asks you to install Hex:
sudo -Hu fuck-gab mix deps.get --only prod
MIX_ENV=prod mix compile

## After that, generate the configuration:
sudo -Hu fuck-gab MIX_ENV=prod mix fuck_gab.config gen

## Check the configuration and make sure it looks alright. Then, rename it for Fuck Gab! to load it (dev.secret.exs for development, prod.secret.exs for production)
mv config/{generated_config.exs,prod.secret.exs}

## The command that generated the configuration also creates the file for creating the PostgreSQL database.
sudo -Hu postgres psql -f config/setup_db.psql

## After that, run the database migration.
sudo -Hu fuck-gab MIX_ENV=prod mix ecto.migrate

## Next, compile the application assets.
npm install --prefix ./assets
npm run deploy --prefix ./assets
mix phx.digest

## Finally, you can start running the server.
sudo -Hu fuck-gab MIX_ENV=prod mix phx.server
```

We provide a sample configuration that you can use to run the page with an Nginx web server in the installation folder.

# Fuck Gab!

Fuck Gab! is a simple information web page designed to help people frustrated with Gab find other alternatives, but can also be used to create other information pages regarding fediverse instances.

## How this site runs

There are two pieces to Fuck Gab!

The first is the backend, which is written in Elixir and uses PostgreSQL for data storage. Because the Elixir programming language is very good at efficiency and low-resource usage, this software is lightweight enough to be run on a Raspberry Pi (with the recommendation that the DB is elsewhere though) but can scale well when run on more powerful hardware.

The second is the frontend, powered by Node.js and located in the assets folder. It only imports the JS for Phoenix framework-related stuff and mostly is comprised of the CSS that makes the final information page seen.

## Copyright

Copyright (C) 2020 Sandia Mesa Animation Studios.

Fuck Gab! is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

Fuck Gab! is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with Fuck Gab!. If not, see https://www.gnu.org/licenses/.

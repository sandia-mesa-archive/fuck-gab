# This file is responsible for configuring your application
# and its dependencies with the aid of the Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
import Config

config :fuck_gab, :site,
  name: "Fuck Gab!",
  description: %{
    content:
      "You may be here because you're sick of Gab's shit. Well, no worries. You're not the only one.
  And there are plenty of alternatives to Gab out there because there's this amazing thing called the fediverse.
  Here's a list of some alternatives on the fediverse that you can try out.",
    display: true,
    content_short:
      "Sick of Gab's shit? There's plenty of alternatives to Gab on the fediverse. Find an alternative fediverse instance right for you here."
  },
  brand_color: "ce2121",
  footer_nav_items: [
    %{
      title: "Source Code",
      url: "https://code.sandiamesa.com/traboone/fuck-gab"
    },
    %{
      title: "License",
      url: "https://code.sandiamesa.com/traboone/fuck-gab/src/branch/master/LICENSE"
    }
  ],
  copyright: "♡ 2020. Copying is an act of love. Please copy and share.",
  suggestion_form: %{
    title: "Suggest an instance",
    description:
      "Have an instance you would like to add to this list? Feel free to suggest it below. All we ask
    is that the instance does not actively encourage or allow activity and content illegal in the United States
    to occur."
  },
  panels: %{
    above_suggestion_form: [
      %{
        title: "Want to start your own instance instead?",
        content: """
        <p>Check out some of this documentation to help you get started:</p>
        <ul>
          <li><a href="https://docs.pleroma.social/backend/installation/debian_based_en">How to install Pleroma on a Debian/Ubuntu server</a></li>
          <li><a href="https://docs.soapbox.pub/frontend/installing/">How to install the Soapbox frontend for Pleroma</a></li>
          <li><a href="https://docs.joinmastodon.org/admin/install/">How to install Mastodon</a></li>
          <li><a href="https://github.com/syuilo/misskey/blob/develop/docs/setup.en.md">How to install and setup Misskey</a></li>
        </ul>
        """
      }
    ]
  }

config :fuck_gab,
  ecto_repos: [FuckGab.Repo]

config :fuck_gab, FuckGab.Scraper, http_client: HTTPoison

config :fuck_gab, FuckGab.Captcha, seconds_valid: 300

# Configures the endpoint
config :fuck_gab, FuckGabWeb.Endpoint,
  url: [host: "localhost"],
  http: [ip: {127, 0, 0, 1}, port: 9002],
  render_errors: [view: FuckGabWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: FuckGab.PubSub

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{config_env()}.exs"

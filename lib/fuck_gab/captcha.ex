# Modified from Pleroma, lib/pleroma/captcha.ex and lib/pleroma/captcha/native.ex
# Copyright © 2017-2020 Pleroma Authors <https://pleroma.social/>
# SPDX-License-Identifier: AGPL-3.0-only

defmodule FuckGab.Captcha do
  alias Calendar.DateTime
  alias Plug.Crypto.KeyGenerator
  alias Plug.Crypto.MessageEncryptor

  @moduledoc """
  Handles CAPTCHA generation and validation of solutions.
  """
  def new do
    new_captcha = generate_new()

    # This make salt a little different for two keys
    {secret, sign_secret} = secret_pair(new_captcha[:token])

    # Basically copy what Phoenix.Token does here, add the time to
    # the actual data and make it a binary to then encrypt it
    encrypted_captcha_answer =
      %{
        at: DateTime.now_utc(),
        answer_data: new_captcha[:answer_data]
      }
      |> :erlang.term_to_binary()
      |> MessageEncryptor.encrypt(secret, sign_secret)

    # Replace the answer with the encrypted answer
    %{new_captcha | answer_data: encrypted_captcha_answer}
  end

  @doc """
  Ask the configured captcha service to validate the captcha
  """
  def validate(token, captcha, answer_data) do
    with {:ok, %{at: at, answer_data: answer_md5}} <- validate_answer_data(token, answer_data),
         :ok <- validate_expiration(at),
         :ok <- check_answer(token, captcha, answer_md5) do
      :ok
    end
  end

  defp seconds_valid, do: FuckGab.Config.get!([__MODULE__, :seconds_valid])

  defp secret_pair(token) do
    secret_key_base = FuckGab.Config.get!([FuckGabWeb.Endpoint, :secret_key_base])
    secret = KeyGenerator.generate(secret_key_base, token <> "_encrypt")
    sign_secret = KeyGenerator.generate(secret_key_base, token <> "_sign")

    {secret, sign_secret}
  end

  defp validate_answer_data(token, answer_data) do
    {secret, sign_secret} = secret_pair(token)

    with false <- is_nil(answer_data),
         {:ok, data} <- MessageEncryptor.decrypt(answer_data, secret, sign_secret),
         %{at: at, answer_data: answer_md5} <- :erlang.binary_to_term(data) do
      {:ok, %{at: at, answer_data: answer_md5}}
    else
      _ -> {:error, :invalid_answer_data}
    end
  end

  defp validate_expiration(created_at) do
    # If the time found is less than (current_time-seconds_valid) then the time has already passed
    # Later we check that the time found is more than the presumed invalidatation time, that means
    # that the data is still valid and the captcha can be checked

    valid_if_after = DateTime.subtract!(DateTime.now_utc(), seconds_valid())

    if DateTime.before?(created_at, valid_if_after) do
      {:error, :expired}
    else
      :ok
    end
  end

  defp generate_new do
    case Captcha.get() do
      {:ok, answer_data, img_binary} ->
        %{
          token: token(),
          url: "data:image/png;base64," <> Base.encode64(img_binary),
          answer_data: answer_data,
          seconds_valid: seconds_valid()
        }
    end
  end

  defp token do
    10
    |> :crypto.strong_rand_bytes()
    |> Base.url_encode64(padding: false)
  end

  defp check_answer(_token, captcha, captcha) when not is_nil(captcha), do: :ok

  defp check_answer(_token, _captcha, _answer), do: {:error, :invalid}
end

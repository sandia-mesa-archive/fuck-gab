defmodule FuckGab.Instance do
  @moduledoc """
  Represents an instance that is, was, or will be listed on the site.
  """
  use Ecto.Schema
  import Ecto.Changeset
  import Ecto.Query
  alias FuckGab.Instance
  alias FuckGab.Repo
  alias FuckGab.Scraper

  @primary_key {:id, FlakeId.Ecto.Type, autogenerate: true}
  # Source of @domain_regex: https://www.regextester.com/93928

  @domain_regex ~r/^(?!:\/\/)([a-zA-Z0-9-_]+\.)*[a-zA-Z0-9][a-zA-Z0-9-_]+\.[a-zA-Z]{2,11}?$/

  @software_tags ["mastodon", "misskey", "pleroma"]

  @derive {Jason.Encoder, only: [:domain, :data]}
  schema "instances" do
    field :domain, :string, null: false
    field :data, :map, default: %{}
    field :software, :string
    field :is_approved, :boolean, default: true
    field :suggestion_reason, :string, default: nil

    timestamps()
  end

  @doc false
  def changeset(struct, params) do
    struct
    |> cast(params, [
      :domain,
      :data,
      :software,
      :is_approved,
      :suggestion_reason
    ])
    |> validate_required([:domain])
    |> check_domain_unique_constraint()
    |> validate_format(:domain, @domain_regex)
  end

  def register_changeset(struct, params) do
    struct
    |> cast(params, [
      :domain,
      :data,
      :software,
      :is_approved,
      :suggestion_reason
    ])
    |> validate_required([:domain])
    |> check_domain_unique_constraint()
    |> validate_format(:domain, @domain_regex)
    |> validate_instance()
  end

  def register(%Ecto.Changeset{} = changeset) do
    with {:ok, instance} <- Repo.insert(changeset) do
      {:ok, instance}
    else
      {:error, changeset} ->
        {:error, changeset}
    end
  end

  def update(changeset) do
    with {:ok, instance} <- Repo.update(changeset) do
      {:ok, instance}
    else
      {:error, changeset} ->
        {:error, changeset}
    end
  end

  def delete(instance) do
    with {:ok, instance} <- Repo.delete(instance) do
      {:ok, instance}
    else
      {:error, changeset} ->
        {:error, changeset}
    end
  end

  def validate_instance(changeset) do
    validate_change(changeset, :domain, fn :domain, domain ->
      with {:ok, _fetched_data} <- Scraper.scrape_validate(domain) do
        []
      else
        _ -> [domain: "is not an instance"]
      end
    end)
  end

  def get_by_domain(domain) when is_binary(domain) do
    Instance
    |> where(domain: ^domain)
    |> Repo.one()
  end

  def filter("", per_page, software, page_number) do
    software = if software == [], do: @software_tags, else: software

    Instance
    |> where(is_approved: true)
    |> where([q], q.software in ^software)
    |> order_by(
      [_q],
      fragment("""
      CASE
        WHEN data->>'title' IS NOT NULL THEN data->>'title'
        ELSE data->>'name'
      END
      """)
    )
    |> Repo.paginate(page: page_number, page_size: per_page)
  end

  def filter(search, per_page, software, page_number) when is_binary(search) do
    search = "%#{search}%"
    software = if software == [], do: @software_tags, else: software

    Instance
    |> where(
      [_q],
      fragment(
        """
        ((data->>'title' ILIKE ?) OR
        (data->>'name' ILIKE ?) OR
        (domain ILIKE ?) OR
        (software ILIKE ?)) AND
        (is_approved = true)
        """,
        ^search,
        ^search,
        ^search,
        ^search
      )
    )
    |> where([q], q.software in ^software)
    |> order_by(
      [_q],
      fragment("""
      CASE
        WHEN data->>'title' IS NOT NULL THEN data->>'title'
        ELSE data->>'name'
      END
      """)
    )
    |> Repo.paginate(page: page_number, page_size: per_page)
  end

  def filter do
    Instance
    |> where(is_approved: true)
    |> Repo.all()
  end

  def filter(is_approved) when is_boolean(is_approved) do
    Instance
    |> where(is_approved: ^is_approved)
    |> Repo.all()
  end

  def get_url(%Instance{domain: domain}) do
    "https://#{domain}"
  end

  defp check_domain_unique_constraint(changeset) do
    error_msg = get_unique_constraint_error(changeset)

    unique_constraint(changeset, :domain, message: error_msg)
  end

  defp get_unique_constraint_error(changeset) do
    with domain when is_binary(domain) <- get_field(changeset, :domain),
         %Instance{} = instance when instance.is_approved == true <-
           Instance.get_by_domain(domain) do
      "already exists in list"
    else
      _ -> "has already been suggested and is pending approval"
    end
  end
end

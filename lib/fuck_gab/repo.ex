defmodule FuckGab.Repo do
  @moduledoc """
  All PostgreSQL (for database) and Scrivener (for search pagination) interactions occur through the Repo.
  """
  use Ecto.Repo,
    otp_app: :fuck_gab,
    adapter: Ecto.Adapters.Postgres

  use Scrivener,
    page_size: 5
end

defmodule FuckGab.Scraper do
  @moduledoc """
  Scrapes remote instance data for display in the Explore area.
  Aspects of an instance (aside from the domain name) are configured
  by the instance itself. So, we need a way to bring it in and determine
  what type of software the instance is running (i.e. Pleroma, Mastodon, Misskey).
  """
  alias FuckGab.Instance

  @http_client Application.get_env(:fuck_gab, FuckGab.Scraper)[:http_client]

  def scrape_instance(%Instance{} = instance) do
    data = build_remote_data(instance)
    software = determine_software(data, instance.software)

    new_attrs = %{data: data, software: software}

    Instance.changeset(instance, new_attrs)
    |> Instance.update()
  end

  def scrape_validate(domain) do
    instance = %Instance{domain: domain}

    with {:ok, fetched_data} <- fetch_instance(instance) do
      {:ok, fetched_data}
    end
  end

  defp build_remote_data(%Instance{data: data} = instance) do
    with {:ok, fetched_data} <- fetch_instance(instance) do
      fetched_data
    else
      _ -> data
    end
  end

  defp determine_software(data, software) do
    with %{
           "title" => _instance_title,
           "version" => software_version
         } <- data do
      if String.contains?(software_version, "Pleroma") do
        "pleroma"
      else
        "mastodon"
      end
    else
      %{
        "name" => _instance_title
      } ->
        "misskey"

      _ ->
        software
    end
  end

  def fetch_instance(%Instance{} = instance) do
    with {:ok, json} <- request_json(:get, "#{Instance.get_url(instance)}/api/v1/instance") do
      {:ok, json}
    else
      _ ->
        with {:ok, json} <- request_json(:post, "#{Instance.get_url(instance)}/api/meta") do
          {:ok, json}
        else
          _ ->
            {:error,
             "#{Instance.get_url(instance)} is not a Mastodon, Pleroma, or Misskey server."}
        end
    end
  end

  defp request_json(request_type, url) do
    with {:ok, res} <- @http_client.request(request_type, url),
         %HTTPoison.Response{body: body} <- res,
         {:ok, json} <- Jason.decode(body) do
      {:ok, json}
    end
  end
end

defmodule FuckGabWeb.PageController do
  use FuckGabWeb, :controller

  def index(conn, _params) do
    brand_color_config = %Chameleon.Hex{hex: FuckGab.Config.get([:site, :brand_color])}

    assigns = %{
      header_title: FuckGab.Config.get([:site, :name]),
      site_name: FuckGab.Config.get([:site, :name]),
      site_description: FuckGab.Config.get([:site, :description]),
      brand_color: Chameleon.convert(brand_color_config, Chameleon.HSL),
      site_footer_nav_items: FuckGab.Config.get([:site, :footer_nav_items]),
      copyright: FuckGab.Config.get([:site, :copyright]),
      panels: FuckGab.Config.get([:site, :panels])
    }

    render(conn, "index.html", assigns)
  end
end

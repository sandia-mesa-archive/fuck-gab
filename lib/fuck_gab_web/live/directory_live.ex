defmodule FuckGabWeb.DirectoryLive do
  @moduledoc """
  Provides a frontend user interface for browsing and searching listed instances that are approved.
  """
  use FuckGabWeb, :live_view
  alias FuckGab.Instance

  def mount(_session, _, socket) do
    assigns = get_search_and_assign_page("", 5, [], 1)

    socket =
      socket
      |> assign(assigns)
      |> assign(:show_advanced, false)
      |> assign(:conn, socket)

    {:ok, socket}
  end

  def handle_event(
        "search",
        %{"search" => search, "per_page" => per_page, "software" => software} = _value,
        socket
      ) do
    assigns = get_search_and_assign_page(search, per_page, software, 1)
    {:noreply, assign(socket, assigns)}
  end

  def handle_event(
        "search",
        %{"search" => search, "per_page" => per_page} = _value,
        socket
      ) do
    assigns = get_search_and_assign_page(search, per_page, [], 1)
    {:noreply, assign(socket, assigns)}
  end

  def handle_event("search", %{"search" => search} = _value, socket) do
    assigns =
      get_search_and_assign_page(search, socket.assigns.per_page, socket.assigns.software, 1)

    {:noreply, assign(socket, assigns)}
  end

  def handle_event("search", _, socket) do
    assigns = get_search_and_assign_page("", socket.assigns.per_page, socket.assigns.software, 1)
    {:noreply, assign(socket, assigns)}
  end

  def handle_event("prev-page", _, socket) do
    assigns =
      get_search_and_assign_page(
        socket.assigns.search,
        socket.assigns.per_page,
        socket.assigns.software,
        socket.assigns.page_number - 1
      )

    {:noreply, assign(socket, assigns)}
  end

  def handle_event("next-page", _, socket) do
    assigns =
      get_search_and_assign_page(
        socket.assigns.search,
        socket.assigns.per_page,
        socket.assigns.software,
        socket.assigns.page_number + 1
      )

    {:noreply, assign(socket, assigns)}
  end

  def handle_event("toggle_advanced", _, socket) do
    {:noreply, update(socket, :show_advanced, &(!&1))}
  end

  defp get_search_and_assign_page(search, per_page, software, page_number) do
    %{
      entries: entries,
      page_number: page_number,
      total_pages: total_pages
    } = Instance.filter(search, per_page, software, page_number)

    [
      instances: entries,
      per_page: per_page,
      software: software,
      page_number: page_number,
      search: search,
      total_pages: total_pages
    ]
  end
end

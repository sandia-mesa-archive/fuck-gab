defmodule FuckGabWeb.SuggestionForm do
  @moduledoc """
  Provides a frontend user interface for others to submit suggestions for instances.
  """
  use FuckGabWeb, :live_view
  alias FuckGab.Instance
  alias FuckGab.Captcha

  def mount(_session, _, socket) do
    socket =
      socket
      |> assign(:conn, socket)
      |> assign(:suggestion_form, FuckGab.Config.get([:site, :suggestion_form]))
      |> assign(:success, nil)
      |> assign(:captcha_error, nil)
      |> assign(:changeset, Instance.changeset(%Instance{}, %{}))
      |> assign(:captcha, Captcha.new())

    {:ok, socket}
  end

  def handle_event("validate", %{"instance" => params}, socket) do
    changeset =
      %Instance{}
      |> Instance.changeset(params)
      |> Map.put(:action, :insert)

    {:noreply, assign(socket, changeset: changeset)}
  end

  def handle_event(
        "save",
        %{"instance" => instance_params, "captcha_answer" => captcha_answer},
        socket
      ) do
    changeset =
      Instance.register_changeset(%Instance{}, instance_params)
      |> Ecto.Changeset.change(is_approved: false)

    token = socket.assigns[:captcha][:token]
    answer_data = socket.assigns[:captcha][:answer_data]

    with :ok <- Captcha.validate(token, captcha_answer, answer_data),
         {:ok, instance} <- Instance.register(changeset) do
      {:noreply,
       assign(socket,
         success: "#{Instance.get_url(instance)} has been submitted for review.",
         changeset: Instance.changeset(%Instance{}, %{}),
         captcha: Captcha.new()
       )}
    else
      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, changeset: changeset, captcha: Captcha.new())}

      {:error, :invalid} ->
        assigns = [
          captcha: Captcha.new(),
          captcha_error: "Invalid CAPTCHA"
        ]

        {:noreply, assign(socket, assigns)}
    end
  end

  def handle_event(
        "refresh_captcha",
        %{
          "captcha" => %{
            "url" => url,
            "answer_data" => answer_data,
            "seconds_valid" => seconds_valid,
            "token" => token
          }
        },
        socket
      ) do
    {:noreply,
     assign(socket,
       captcha: %{url: url, answer_data: answer_data, seconds_valid: seconds_valid, token: token}
     )}
  end

  def handle_event("refresh_captcha", _, socket) do
    {:noreply, assign(socket, captcha: Captcha.new())}
  end
end

# Modified from Pleroma, lib/mix/pleroma.ex
# Copyright © 2017-2020 Pleroma Authors <https://pleroma.social/>
# SPDX-License-Identifier: AGPL-3.0-only

defmodule Mix.FuckGab do
  @apps [
    :restarter,
    :ecto,
    :ecto_sql,
    :postgrex,
    :db_connection,
    :hackney,
    :flake_id
  ]

  @doc "Common functions to be reused in mix tasks"
  def start_fuck_gab do
    Application.put_env(:phoenix, :serve_endpoints, false, persistent: true)

    Enum.each(@apps, &Application.ensure_all_started/1)

    children = [
      FuckGab.Repo,
      FuckGabWeb.Telemetry,
      {Phoenix.PubSub, name: FuckGab.PubSub},
      FuckGabWeb.Endpoint
    ]

    opts = [strategy: :one_for_one, name: FuckGab.Supervisor]
    Supervisor.start_link(children, opts)
  end

  @doc "Outputs a shell message"
  def shell_info(message) do
    if mix_shell?(Mix.env(), FuckGab.Config.get(:mix_shell)),
      do: Mix.shell().info(message),
      else: IO.puts(message)
  end

  @doc "Outputs a prompt for a y/n question."
  def shell_yes?(message) do
    shell_prompt(message, "n", "Yn") in ~w(Yn Y y)
  end

  @doc "A shell prompt function for when mix_shell is not available."
  def shell_prompt(prompt, defval \\ nil, defname \\ nil) do
    prompt_message = "#{prompt} [#{defname || defval}]"

    input =
      if mix_shell?(Mix.env(), FuckGab.Config.get(:mix_shell)),
        do: Mix.shell().prompt(prompt_message),
        else: :io.get_line(prompt_message)

    case input do
      "\n" ->
        case defval do
          nil ->
            shell_prompt(prompt, defval, defname)

          defval ->
            defval
        end

      input ->
        String.trim(input)
    end
  end

  @doc "A function to output an error in the shell prompt."
  def shell_error(message) do
    if mix_shell?(Mix.env(), FuckGab.Config.get(:mix_shell)),
      do: Mix.shell().error(message),
      else: IO.puts(:stderr, message)
  end

  @doc "A safe check to see if `Mix.shell/0` is available"
  def mix_shell?(env, mix_shell_config) when env == :test and mix_shell_config == false do
    false
  end

  def mix_shell?(_env, _mix_shell_config) do
    :erlang.function_exported(Mix, :shell, 0)
  end

  @doc "A function for getting options"
  def get_option(options, opt, prompt, defval \\ nil, defname \\ nil) do
    Keyword.get(options, opt) || shell_prompt(prompt, defval, defname)
  end
end

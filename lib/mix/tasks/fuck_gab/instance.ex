defmodule Mix.Tasks.FuckGab.Instance do
  import Mix.FuckGab
  use Mix.Task

  alias FuckGab.Instance
  alias FuckGab.Scraper

  @shortdoc "Manual tasks for instance listings"
  def run(["add", domain]) do
    start_fuck_gab()

    attrs = %{
      domain: domain
    }

    instance =
      Instance.register_changeset(%Instance{}, attrs)
      |> Instance.register()

    with {:ok, instance} <- instance do
      Scraper.scrape_instance(instance)
      shell_info("Listing for #{get_url(domain)} created!")
    else
      _ -> shell_error("#{get_url(domain)} is not an instance.")
    end
  end

  def run(["rm", domain]) do
    start_fuck_gab()

    with %Instance{} = instance <- Instance.get_by_domain(domain) do
      proceed? =
        shell_yes?(
          "Are you sure you want to remove the listing for #{get_url(domain)}? (THIS CANNOT BE UNDONE!!)"
        )

      if proceed? do
        {:ok, _instance} = Instance.delete(instance)
        shell_info("Deleted listing for #{get_url(domain)}")
      end
    else
      _ -> shell_error("No listing for #{get_url(domain)}")
    end
  end

  def run(["pending"]) do
    start_fuck_gab()

    with instances when is_list(instances) and length(instances) > 0 <- Instance.filter(false) do
      shell_info("""
      There #{if length(instances) > 1, do: "are", else: "is"} #{length(instances)} #{
        if length(instances) > 1, do: "instances", else: "instance"
      } pending approval:
      """)

      Enum.each(instances, fn instance -> shell_info("- #{Instance.get_url(instance)}") end)
    else
      _ -> shell_info("No instances pending approval.")
    end
  end

  def run(["approve", domain]) do
    start_fuck_gab()

    with %Instance{} = instance when instance.is_approved == false <-
           Instance.get_by_domain(domain) do
      new_attrs = %{is_approved: true}

      {:ok, instance} =
        Instance.changeset(instance, new_attrs)
        |> Instance.update()

      Scraper.scrape_instance(instance)

      shell_info("Approved listing for #{get_url(domain)}")
    else
      _ ->
        with %Instance{} = instance when instance.is_approved == true <-
               Instance.get_by_domain(domain) do
          shell_error("Instance has already been approved.")
        else
          _ -> shell_error("No listing for #{get_url(domain)}")
        end
    end
  end

  defp get_url(domain) when is_binary(domain) do
    "https://#{domain}"
  end
end

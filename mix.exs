defmodule FuckGab.MixProject do
  use Mix.Project

  def project do
    [
      app: :fuck_gab,
      version: "0.9.0",
      elixir: "~> 1.7",
      elixirc_paths: elixirc_paths(Mix.env()),
      compilers: [:phoenix, :gettext] ++ Mix.compilers(),
      start_permanent: Mix.env() == :prod,
      aliases: aliases(),
      deps: deps(),
      test_coverage: [tool: ExCoveralls],
      # Docs
      name: "Fuck Gab!",
      homepage_url: "https://fuckgab.com",
      source_url: "https://code.sandiamesa.com/traboone/fuck-gab",
      docs: [
        source_url_pattern:
          "https://code.sandiamesa.com/traboone/fuck-gab/src/branch/master/%{path}#L%{line}",
        logo: "assets/static/favicon.png"
      ]
    ]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {FuckGab.Application, []},
      extra_applications: [:logger, :runtime_tools]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      {:phoenix, "~> 1.5.6"},
      {:phoenix_ecto, "~> 4.1"},
      {:ecto_sql, "~> 3.4"},
      {:postgrex, ">= 0.0.0"},
      {:phoenix_html, "~> 2.11"},
      {:phoenix_live_reload, "~> 1.2", only: :dev},
      {:phoenix_live_dashboard, "~> 0.3 or ~> 0.2.9"},
      {:phoenix_live_view, "~> 0.15.0"},
      {:floki, ">= 0.0.0", only: :test},
      {:telemetry_metrics, "~> 0.4"},
      {:telemetry_poller, "~> 0.4"},
      {:gettext, "~> 0.11"},
      {:jason, "~> 1.0"},
      {:httpoison, "~> 1.7"},
      {:plug_cowboy, "~> 2.0"},
      {:flake_id, "~> 0.1.0"},
      {:ex_machina, "~> 2.4", only: :test},
      {:chameleon, "~> 2.3"},
      {:timex, "~> 3.6"},
      {:scrivener_ecto, "~> 2.7"},
      {:excoveralls, "~> 0.13.3", only: :test},
      {:ex_doc, "~> 0.23.0", only: :dev, runtime: false},
      {:captcha,
       git: "https://git.pleroma.social/pleroma/elixir-libraries/elixir-captcha.git",
       ref: "e0f16822d578866e186a0974d65ad58cddc1e2ab"},
      {:calendar, "~> 1.0"},
      {:plug_crypto, "~> 1.2.2"},
      {:fast_sanitize, "~> 0.2.2"}
    ]
  end

  # Aliases are shortcuts or tasks specific to the current project.
  # For example, to install project dependencies and perform other setup tasks, run:
  #
  #     $ mix setup
  #
  # See the documentation for `Mix` for more info on aliases.
  defp aliases do
    [
      setup: ["deps.get", "ecto.setup", "cmd npm install --prefix assets"],
      "compile.frontend": ["cmd rm -rf priv/static", "cmd npm run deploy --prefix ./assets", "phx.digest"],
      "ecto.setup": ["ecto.create", "ecto.migrate", "run priv/repo/seeds.exs"],
      "ecto.reset": ["ecto.drop", "ecto.setup"],
      test: ["ecto.create --quiet", "ecto.migrate --quiet", "test"]
    ]
  end
end

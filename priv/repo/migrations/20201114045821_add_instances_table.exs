defmodule FuckGab.Repo.Migrations.AddInstancesTable do
  use Ecto.Migration

  def change do
    create_if_not_exists table(:instances) do
      add :domain, :string
      add :data, :map

      timestamps()
    end

    create_if_not_exists unique_index(:instances, [:domain])
  end
end

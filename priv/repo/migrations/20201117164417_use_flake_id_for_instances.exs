defmodule FuckGab.Repo.Migrations.UseFlakeIdForInstances do
  use Ecto.Migration

  def change do
    # Doesn't really matter that we're dropping and re-creating this table currently just for making
    # the primary_key in the instances table a Flake ID. It's easier and we're still pretty early-on
    # in the development.
    drop_if_exists table(:instances)

    create_if_not_exists table(:instances, primary_key: false) do
      add :id, :uuid, primary_key: true
      add :domain, :string, null: false
      add :data, :map, default: %{}

      timestamps()
    end

    create_if_not_exists unique_index(:instances, [:domain])
  end
end

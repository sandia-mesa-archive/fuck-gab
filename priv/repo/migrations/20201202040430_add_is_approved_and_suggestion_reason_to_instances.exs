defmodule FuckGab.Repo.Migrations.AddIsApprovedAndSuggestionReasonToInstances do
  use Ecto.Migration

  def change do
    alter table(:instances) do
      add(:is_approved, :boolean, default: true)
      add(:suggestion_reason, :string, default: nil)
    end
  end
end

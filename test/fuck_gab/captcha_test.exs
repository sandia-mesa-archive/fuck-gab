# Modified from Pleroma, test/pleroma/captcha_test.exs
# Copyright © 2017-2020 Pleroma Authors <https://pleroma.social/>
# SPDX-License-Identifier: AGPL-3.0-only

defmodule FuckGab.CaptchaTest do
  use FuckGabWeb.ConnCase

  alias FuckGab.Captcha

  describe "Captcha" do
    test "new and validate" do
      new = Captcha.new()

      assert %{
               answer_data: answer,
               token: token,
               url: "data:image/png;base64," <> _,
               seconds_valid: 300
             } = new

      {:ok, %{at: _at, answer_data: answer_md5}} = decrypt_answer_data(token, answer)

      assert is_binary(answer)
      assert :ok = Captcha.validate(token, answer_md5, answer)

      assert {:error, :invalid_answer_data} ==
               Captcha.validate(token, answer_md5, answer <> "foobar")

      assert {:error, :invalid} == Captcha.validate(token, answer_md5 <> "foobar", answer)
    end
  end
end

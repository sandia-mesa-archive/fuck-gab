# Partially modified from FuckGab, test/pleroma/config_test.exs
# Copyright © 2017-2020 FuckGab Authors <https://pleroma.social/>
# SPDX-License-Identifier: AGPL-3.0-only

defmodule FuckGab.ConfigTest do
  use ExUnit.Case

  test "get/1 with default" do
    assert FuckGab.Config.get([:site, :bird_up], "Bird Up!") == "Bird Up!"
  end

  test "get/1 with an atom" do
    assert FuckGab.Config.get(:site) == Application.get_env(:fuck_gab, :site)
    assert FuckGab.Config.get(:azertyuiop) == nil
    assert FuckGab.Config.get(:azertyuiop, true) == true
  end

  test "get/1 with a list of keys" do
    assert FuckGab.Config.get([:site, :name]) ==
             Keyword.get(Application.get_env(:fuck_gab, :site), :name)

    assert FuckGab.Config.get([FuckGabWeb.Endpoint, :render_errors, :view]) ==
             get_in(
               Application.get_env(
                 :fuck_gab,
                 FuckGabWeb.Endpoint
               ),
               [:render_errors, :view]
             )

    assert FuckGab.Config.get([:azerty, :uiop]) == nil
    assert FuckGab.Config.get([:azerty, :uiop], true) == true
  end

  describe "nil values" do
    setup do
      FuckGab.Config.put(:lorem, nil)
      FuckGab.Config.put(:ipsum, %{dolor: [sit: nil]})
      FuckGab.Config.put(:dolor, sit: %{amet: nil})

      on_exit(fn -> Enum.each(~w(lorem ipsum dolor)a, &FuckGab.Config.delete/1) end)
    end

    test "get/1 with an atom for nil value" do
      assert FuckGab.Config.get(:lorem) == nil
    end

    test "get/2 with an atom for nil value" do
      assert FuckGab.Config.get(:lorem, true) == nil
    end

    test "get/1 with a list of keys for nil value" do
      assert FuckGab.Config.get([:ipsum, :dolor, :sit]) == nil
      assert FuckGab.Config.get([:dolor, :sit, :amet]) == nil
    end

    test "get/2 with a list of keys for nil value" do
      assert FuckGab.Config.get([:ipsum, :dolor, :sit], true) == nil
      assert FuckGab.Config.get([:dolor, :sit, :amet], true) == nil
    end
  end

  test "get/1 with a single key" do
    assert FuckGab.Config.get([:simpsons], nil) == nil
  end

  test "get/1 when value is false" do
    FuckGab.Config.put([:instance, :false_test], false)
    FuckGab.Config.put([:instance, :nested], [])
    FuckGab.Config.put([:instance, :nested, :false_test], false)

    assert FuckGab.Config.get([:instance, :false_test]) == false
    assert FuckGab.Config.get([:instance, :nested, :false_test]) == false
  end

  test "get!/1" do
    assert FuckGab.Config.get!(:site) == Application.get_env(:fuck_gab, :site)

    assert FuckGab.Config.get!([:site, :name]) ==
             Keyword.get(Application.get_env(:fuck_gab, :site), :name)

    assert_raise(FuckGab.Config.Error, fn ->
      FuckGab.Config.get!(:azertyuiop)
    end)

    assert_raise(FuckGab.Config.Error, fn ->
      FuckGab.Config.get!([:azerty, :uiop])
    end)
  end

  test "get!/1 when value is false" do
    FuckGab.Config.put([:instance, :false_test], false)
    FuckGab.Config.put([:instance, :nested], [])
    FuckGab.Config.put([:instance, :nested, :false_test], false)

    assert FuckGab.Config.get!([:instance, :false_test]) == false
    assert FuckGab.Config.get!([:instance, :nested, :false_test]) == false
  end

  test "put/2 with a key" do
    FuckGab.Config.put(:config_test, true)

    assert FuckGab.Config.get(:config_test) == true
  end

  test "put/2 with a list of keys" do
    FuckGab.Config.put([:instance, :config_test], true)
    FuckGab.Config.put([:instance, :config_nested_test], [])
    FuckGab.Config.put([:instance, :config_nested_test, :x], true)

    assert FuckGab.Config.get([:instance, :config_test]) == true
    assert FuckGab.Config.get([:instance, :config_nested_test, :x]) == true
  end

  test "delete/1 with a key" do
    FuckGab.Config.put([:delete_me], :delete_me)
    FuckGab.Config.delete([:delete_me])
    assert FuckGab.Config.get([:delete_me]) == nil
  end

  test "delete/2 with a list of keys" do
    FuckGab.Config.put([:delete_me], hello: "world", world: "Hello")
    FuckGab.Config.delete([:delete_me, :world])
    assert FuckGab.Config.get([:delete_me]) == [hello: "world"]
    FuckGab.Config.put([:delete_me, :delete_me], hello: "world", world: "Hello")
    FuckGab.Config.delete([:delete_me, :delete_me, :world])
    assert FuckGab.Config.get([:delete_me, :delete_me]) == [hello: "world"]

    assert FuckGab.Config.delete([:this_key_does_not_exist])
    assert FuckGab.Config.delete([:non, :existing, :key])
  end

  test "fetch/1" do
    FuckGab.Config.put([:lorem], :ipsum)
    FuckGab.Config.put([:ipsum], dolor: :sit)

    assert FuckGab.Config.fetch([:lorem]) == {:ok, :ipsum}
    assert FuckGab.Config.fetch(:lorem) == {:ok, :ipsum}
    assert FuckGab.Config.fetch([:ipsum, :dolor]) == {:ok, :sit}
    assert FuckGab.Config.fetch([:lorem, :ipsum]) == :error
    assert FuckGab.Config.fetch([:loremipsum]) == :error
    assert FuckGab.Config.fetch(:loremipsum) == :error

    FuckGab.Config.delete([:lorem])
    FuckGab.Config.delete([:ipsum])
  end
end

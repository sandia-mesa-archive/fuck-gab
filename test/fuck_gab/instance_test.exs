defmodule FuckGab.InstanceTest do
  alias FuckGab.Instance
  alias FuckGab.Repo
  use FuckGab.DataCase
  import FuckGab.Factory

  test "create instance" do
    attrs = %{
      domain: "traboone.com"
    }

    {:ok, %Instance{} = instance} =
      Instance.register_changeset(%Instance{}, attrs)
      |> Instance.register()

    assert instance.domain == "traboone.com"
  end

  test "update an instance" do
    instance = insert(:instance, domain: "traboone.com")

    new_attrs = %{
      data: json_file("test/fixtures/traboone-instance.json")
    }

    {:ok, %Instance{} = instance} =
      Instance.changeset(instance, new_attrs)
      |> Instance.update()

    with %Instance{} = instance <- Repo.reload(instance) do
      assert instance.data == json_file("test/fixtures/traboone-instance.json")
    end
  end

  test "delete instance" do
    instance = insert(:instance, domain: "traboone.com")

    with {:ok, instance} <- Instance.delete(instance) do
      assert Repo.reload(instance) == nil
    end
  end

  test "get an instance by domain" do
    instance = insert(:instance, domain: "traboone.com")

    assert instance == Instance.get_by_domain("traboone.com")
  end

  test "search an instance" do
    instance = insert(:instance, domain: "traboone.com")

    new_attrs = %{
      data: json_file("test/fixtures/traboone-instance.json"),
      software: "pleroma"
    }

    {:ok, %Instance{} = instance} =
      Instance.changeset(instance, new_attrs)
      |> Instance.update()

    with %Instance{} = instance <- Repo.reload(instance) do
      search = Instance.filter(instance.domain, 5, [], 1)
      assert search.entries == [instance]
    end
  end

  # Function to read a JSON file
  defp json_file(filename) do
    filename
    |> File.read!()
    |> Jason.decode!()
  end
end

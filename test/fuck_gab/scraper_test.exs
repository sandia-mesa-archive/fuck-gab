defmodule FuckGab.ScraperTest do
  alias FuckGab.Scraper
  alias FuckGab.Repo
  use FuckGab.DataCase
  import FuckGab.Factory

  test "scraper succeeds for a Pleroma instance" do
    instance = insert(:instance, domain: "traboone.com")

    Scraper.scrape_instance(instance)
    instance = Repo.reload(instance)

    assert instance.data ==
             json_file("test/fixtures/traboone-instance.json")

    assert instance.software == "pleroma"
  end

  test "scraper succeeds for a Mastodon instance" do
    instance = insert(:instance, domain: "gameliberty.club")

    Scraper.scrape_instance(instance)
    instance = Repo.reload(instance)

    assert instance.data ==
             json_file("test/fixtures/game-liberty-club-instance.json")

    assert instance.software == "mastodon"
  end

  test "scraper succeeds for a Misskey instance" do
    instance = insert(:instance, domain: "skippers-bin.com")

    Scraper.scrape_instance(instance)
    instance = Repo.reload(instance)

    assert instance.data ==
             json_file("test/fixtures/skippers-bin-meta.json")

    assert instance.software == "misskey"
  end

  test "scraper fails for a non-fediverse instance" do
    instance = insert(:instance, domain: "duckduckgo.com")

    Scraper.scrape_instance(instance)
    instance = Repo.reload(instance)

    assert instance.data == %{}
    assert instance.software == nil
  end

  # Function to read a JSON file
  defp json_file(filename) do
    filename
    |> File.read!()
    |> Jason.decode!()
  end
end

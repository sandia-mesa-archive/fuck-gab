defmodule FuckGab.ScrubberTest do
  use FuckGab.DataCase

  describe "Scraper" do
    test "make sure HTML Scrubber works properly" do
      expected = """
        <b>bold up</b>
        <p>paragraphics</p>
        break down<br/>
        <span>spannity span, wonderful spaaan</span>
        <ul><li>Unordered list stuff</li></ul>
        <ol><li>ORDER! ORDER!</li></ol>
        <blockquote>Blocked by the quotes</blockquote>
        <abbr title="This is Sparta">Welcome to Sparta</abbr>
        <code>Oh, hi! I&#39;m Cody!</code>
        <del>DELET THIS!</del>
        <em>Emerald</em>
        <i>Mama-mia!</i>
        <pre>Prepare</pre>
        <strong>STRUNGGG MAN!</strong>
        <sub>Subway in New York</sub>
        <sup>Sup bitches!</sup>
        <u>Why r u gae?</u>
      """

      assert {:ok, expected} == FastSanitize.Sanitizer.scrub(expected, FuckGab.Scrubber)
    end
  end
end

defmodule FuckGabWeb.DirectoryLiveTest do
  use FuckGabWeb.ConnCase
  alias FuckGab.Scraper
  alias FuckGab.Repo
  alias FuckGab.Instance
  import Phoenix.LiveViewTest
  import FuckGab.Factory

  describe "DirectoryLive upon mounting" do
    test "when there's no instances", %{conn: conn} do
      {:ok, _view, disconnected_html} =
        live_isolated(conn, FuckGabWeb.DirectoryLive, session: %{})

      assert disconnected_html =~ "<h5 class=\"no-content\">No instances found.</h5>"
    end

    test "when there's instances", %{conn: conn} do
      instance = insert(:instance, domain: "traboone.com")

      Scraper.scrape_instance(instance)
      instance = Repo.reload(instance)

      {:ok, _view, disconnected_html} =
        live_isolated(conn, FuckGabWeb.DirectoryLive, session: %{})

      assert disconnected_html =~ "<h4 class=\"tab-title\">#{instance.data["title"]}</h4>"

      assert disconnected_html =~
               "<a href=\"#{Instance.get_url(instance)}\">#{instance.domain}</a>"

      assert disconnected_html =~
               "<span class=\"tab-software\"><i class=\"fa fa-#{instance.software}\"></i> #{
                 String.capitalize(instance.software)
               }</span>"
    end
  end

  describe "DirectoryLive searches" do
    test "search for nothing", %{conn: conn} do
      instance = insert(:instance, domain: "traboone.com")

      Scraper.scrape_instance(instance)
      instance = Repo.reload(instance)

      {:ok, view, disconnected_html} = live_isolated(conn, FuckGabWeb.DirectoryLive, session: %{})

      assert render_change(view, :search, %{})

      assert disconnected_html =~ "<h4 class=\"tab-title\">#{instance.data["title"]}</h4>"

      assert disconnected_html =~
               "<a href=\"#{Instance.get_url(instance)}\">#{instance.domain}</a>"

      assert disconnected_html =~
               "<span class=\"tab-software\"><i class=\"fa fa-#{instance.software}\"></i> #{
                 String.capitalize(instance.software)
               }</span>"
    end

    test "search for something", %{conn: conn} do
      instance = insert(:instance, domain: "traboone.com")

      Scraper.scrape_instance(instance)
      instance = Repo.reload(instance)

      {:ok, view, disconnected_html} = live_isolated(conn, FuckGabWeb.DirectoryLive, session: %{})

      assert render_change(view, :search, %{search: "Traboone"})

      assert disconnected_html =~ "<h4 class=\"tab-title\">#{instance.data["title"]}</h4>"

      assert disconnected_html =~
               "<a href=\"#{Instance.get_url(instance)}\">#{instance.domain}</a>"

      assert disconnected_html =~
               "<span class=\"tab-software\"><i class=\"fa fa-#{instance.software}\"></i> #{
                 String.capitalize(instance.software)
               }</span>"
    end
  end

  describe "Advanced searches" do
    test "show advanced search settings", %{conn: conn} do
      {:ok, view, _disconnected_html} =
        live_isolated(conn, FuckGabWeb.DirectoryLive, session: %{})

      disconnected_html = render_click(view, :toggle_advanced, %{})

      assert disconnected_html =~ "<div class=\"advanced-search\">"
    end

    test "change number of instances per page", %{conn: conn} do
      instance = insert(:instance, domain: "gameliberty.club")
      instance2 = insert(:instance, domain: "traboone.com")

      Scraper.scrape_instance(instance)
      Scraper.scrape_instance(instance2)

      instance = Repo.reload(instance)
      instance2 = Repo.reload(instance2)

      {:ok, view, _disconnected_html} =
        live_isolated(conn, FuckGabWeb.DirectoryLive, session: %{})

      render_click(view, :toggle_advanced, %{})

      disconnected_html = render_change(view, :search, %{search: "", software: [], per_page: 1})

      assert disconnected_html =~ "<h4 class=\"tab-title\">#{instance.data["title"]}</h4>"
      assert disconnected_html != "<h4 class=\"tab-title\">#{instance2.data["title"]}</h4>"
    end

    test "filter instances by software", %{conn: conn} do
      instance = insert(:instance, domain: "gameliberty.club")
      instance2 = insert(:instance, domain: "traboone.com")

      Scraper.scrape_instance(instance)
      Scraper.scrape_instance(instance2)

      instance = Repo.reload(instance)
      instance2 = Repo.reload(instance2)

      {:ok, view, _disconnected_html} =
        live_isolated(conn, FuckGabWeb.DirectoryLive, session: %{})

      render_click(view, :toggle_advanced, %{})

      disconnected_html =
        render_change(view, :search, %{search: "", software: ["mastodon"], per_page: 2})

      assert disconnected_html =~ "<h4 class=\"tab-title\">#{instance.data["title"]}</h4>"
      assert disconnected_html != "<h4 class=\"tab-title\">#{instance2.data["title"]}</h4>"
    end
  end

  describe "DirectoryLive pagination" do
    test "navigate to next page", %{conn: conn} do
      {:ok, view, _disconnected_html} =
        live_isolated(conn, FuckGabWeb.DirectoryLive, session: %{})

      assert render_change(view, :"next-page", %{})
    end

    test "navigate to previous page", %{conn: conn} do
      {:ok, view, _disconnected_html} =
        live_isolated(conn, FuckGabWeb.DirectoryLive, session: %{})

      assert render_change(view, :"prev-page", %{})
    end
  end
end

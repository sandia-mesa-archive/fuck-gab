defmodule FuckGabWeb.SuggestionFormTest do
  use FuckGabWeb.ConnCase
  alias FuckGab.Instance
  alias FuckGab.Repo
  alias FuckGab.Captcha
  import Phoenix.LiveViewTest
  import FuckGab.Factory

  describe "SuggestionForm upon mounting" do
    test "upon mount", %{conn: conn} do
      {:ok, _view, disconnected_html} =
        live_isolated(conn, FuckGabWeb.SuggestionForm, session: %{})

      assert disconnected_html =~ "<h3 class=\"tab-title\">Suggest an instance</h3>"
    end
  end

  describe "SuggestionForm validating" do
    test "with no errors", %{conn: conn} do
      {:ok, view, _disconnected_html} =
        live_isolated(conn, FuckGabWeb.SuggestionForm, session: %{})

      disconnected_html = render_change(view, :validate, %{instance: %{domain: "example.com"}})

      assert disconnected_html =~ "value=\"example.com\""
    end

    test "with errors", %{conn: conn} do
      {:ok, view, _disconnected_html} =
        live_isolated(conn, FuckGabWeb.SuggestionForm, session: %{})

      disconnected_html = render_change(view, :validate, %{instance: %{domain: ""}})

      assert disconnected_html =~
               "<span class=\"invalid-feedback\" phx-feedback-for=\"instance_domain\">can&apos;t be blank</span>"
    end
  end

  describe "SuggestionForm submitting" do
    test "with no errors", %{conn: conn} do
      {:ok, view, _disconnected_html} =
        live_isolated(conn, FuckGabWeb.SuggestionForm, session: %{})

      captcha = Captcha.new()

      render_click(view, :refresh_captcha, %{captcha: captcha})

      {:ok, %{at: _at, answer_data: captcha_answer}} =
        decrypt_answer_data(captcha.token, captcha.answer_data)

      disconnected_html =
        render_submit(view, :save, %{
          instance: %{domain: "traboone.com", suggestion_reason: "WHACK OFF!"},
          captcha_answer: captcha_answer
        })

      instance = Instance.get_by_domain("traboone.com")
      assert instance.domain == "traboone.com"
      assert instance.is_approved == false

      assert disconnected_html =~
               "<p class=\"alert alert-success\" role=\"alert\">#{Instance.get_url(instance)} has been submitted for review.</p>"
    end

    test "with errors", %{conn: conn} do
      instance = insert(:instance, domain: "traboone.com", is_approved: false)

      {:ok, view, _disconnected_html} =
        live_isolated(conn, FuckGabWeb.SuggestionForm, session: %{})

      captcha = Captcha.new()

      render_click(view, :refresh_captcha, %{captcha: captcha})

      {:ok, %{at: _at, answer_data: captcha_answer}} =
        decrypt_answer_data(captcha.token, captcha.answer_data)

      disconnected_html =
        render_submit(view, :save, %{
          instance: %{
            domain: "",
            suggestion_reason: "I'm just gonna eat me a big ol' bag o-DICK!"
          },
          captcha_answer: captcha_answer
        })

      assert disconnected_html =~
               "<span class=\"invalid-feedback\" phx-feedback-for=\"instance_domain\">can&apos;t be blank</span>"

      render_click(view, :refresh_captcha, %{captcha: captcha})

      {:ok, %{at: _at, answer_data: captcha_answer}} =
        decrypt_answer_data(captcha.token, captcha.answer_data)

      disconnected_html =
        render_submit(view, :save, %{
          instance: %{
            domain: "duckduckgo.com",
            suggestion_reason: "I'm just gonna eat me a big ol' bag o-DICK!"
          },
          captcha_answer: captcha_answer
        })

      assert disconnected_html =~
               "<span class=\"invalid-feedback\" phx-feedback-for=\"instance_domain\">is not an instance</span>"

      disconnected_html =
        render_submit(view, :save, %{
          instance: %{
            domain: "traboone.com",
            suggestion_reason: "8008$! N0t f0r m3!"
          },
          captcha_answer: captcha_answer <> "bA6"
        })

      assert disconnected_html =~ "<span class=\"invalid-feedback\">Invalid CAPTCHA</span>"

      render_click(view, :refresh_captcha, %{captcha: captcha})

      {:ok, %{at: _at, answer_data: captcha_answer}} =
        decrypt_answer_data(captcha.token, captcha.answer_data)

      disconnected_html =
        render_submit(view, :save, %{
          instance: %{
            domain: "traboone.com",
            suggestion_reason: "Let me in. LET ME IINNNNN!"
          },
          captcha_answer: captcha_answer
        })

      assert disconnected_html =~
               "<span class=\"invalid-feedback\" phx-feedback-for=\"instance_domain\">has already been suggested and is pending approval</span>"

      new_attrs = %{is_approved: true}

      with %Instance{} = instance <- Repo.reload(instance) do
        {:ok, _instance} =
          Instance.changeset(instance, new_attrs)
          |> Instance.update()
      end

      render_click(view, :refresh_captcha, %{captcha: captcha})

      {:ok, %{at: _at, answer_data: captcha_answer}} =
        decrypt_answer_data(captcha.token, captcha.answer_data)

      disconnected_html =
        render_submit(view, :save, %{
          instance: %{
            domain: "traboone.com",
            suggestion_reason: "PLEASEEEEEEEEEEE!!!"
          },
          captcha_answer: captcha_answer
        })

      assert disconnected_html =~
               "<span class=\"invalid-feedback\" phx-feedback-for=\"instance_domain\">already exists in list</span>"
    end
  end
end

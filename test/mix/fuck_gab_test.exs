# Modified from Pleroma, test/mix/pleroma_test.exs
# Copyright © 2017-2020 Pleroma Authors <https://pleroma.social/>
# SPDX-License-Identifier: AGPL-3.0-only

defmodule Mix.FuckGabTest do
  use FuckGabWeb.ConnCase
  import Mix.FuckGab
  import ExUnit.CaptureIO

  setup_all do
    Mix.shell(Mix.Shell.Process)

    on_exit(fn ->
      Mix.shell(Mix.Shell.IO)
    end)

    :ok
  end

  describe "shell_prompt/1" do
    test "input" do
      send(self(), {:mix_shell_input, :prompt, "Yes"})

      answer = shell_prompt("Do you want this?")
      assert_received {:mix_shell, :prompt, [message]}
      assert message =~ "Do you want this?"
      assert answer == "Yes"
    end

    test "when Mix.shell/0 is not available" do
      clear_config([:mix_shell], false)

      capture_io("Yes", fn ->
        input = shell_prompt("Do you want this?")
        IO.write(input)
      end) =~ "Do you want this? Yes\n"
    end

    test "with defval" do
      send(self(), {:mix_shell_input, :prompt, "\n"})
      answer = shell_prompt("Do you want this?", "defval")

      assert_received {:mix_shell, :prompt, [message]}
      assert message =~ "Do you want this? [defval]"
      assert answer == "defval"
    end

    test "with defname" do
      send(self(), {:mix_shell_input, :prompt, "\n"})
      answer = shell_prompt("Do you want this?", "defval", "defname")

      assert_received {:mix_shell, :prompt, [message]}
      assert message =~ "Do you want this? [defname]"
      assert answer == "defval"
    end

    test "with defname and no defval" do
      send(self(), {:mix_shell_input, :prompt, "\n"})
      send(self(), {:mix_shell_input, :prompt, "yes"})
      answer = shell_prompt("Do you want this?", nil, "defname")

      assert_received {:mix_shell, :prompt, [message]}
      assert message =~ "Do you want this? [defname]"
      assert answer == "yes"
    end
  end

  describe "shell_info/1" do
    test "message" do
      _message = shell_info("Test message")

      assert_received {:mix_shell, :info, [message]}
      assert message =~ "Test message"
    end

    test "when Mix.shell/0 is not available" do
      clear_config([:mix_shell], false)

      assert capture_io(fn -> shell_info("Test message") end) == "Test message\n"
    end
  end

  describe "shell_error/1" do
    test "error" do
      _message = shell_error("Test error")

      assert_received {:mix_shell, :error, [message]}
      assert message =~ "Test error"
    end

    test "when Mix.shell/0 is not available" do
      clear_config([:mix_shell], false)

      assert capture_io(:stderr, fn -> shell_error("Test error") end) == "Test error\n"
    end
  end

  describe "get_option/3" do
    test "get from options" do
      assert get_option([domain: "some-domain.com"], :domain, "Promt") == "some-domain.com"
    end

    test "get from prompt" do
      send(self(), {:mix_shell_input, :prompt, "another-domain.com"})
      assert get_option([], :domain, "Prompt") == "another-domain.com"
    end
  end
end

# Modified from Pleroma, test/mix/tasks/pleroma/instance_test.exs
# Copyright © 2017-2020 Pleroma Authors <https://pleroma.social/>
# SPDX-License-Identifier: AGPL-3.0-only

defmodule Mix.Tasks.FuckGab.ConfigTest do
  use FuckGab.DataCase

  @uuid Ecto.UUID.generate()
  defp tmp_path do
    "/tmp/generated_files/#{@uuid}/"
  end

  setup do
    File.mkdir_p!(tmp_path())
    Mix.shell(Mix.Shell.Process)

    on_exit(fn ->
      File.rm_rf(tmp_path())
      Mix.shell(Mix.Shell.IO)
    end)

    :ok
  end

  test "running gen" do
    Mix.Tasks.FuckGab.Config.run([
      "gen",
      "--output",
      tmp_path() <> "generated_config.exs",
      "--output-psql",
      tmp_path() <> "setup.psql",
      "--domain",
      "fuckgab.com",
      "--site-name",
      "Fuck Gab!",
      "--site-description",
      "Test description",
      "--dbhost",
      "dbhost",
      "--dbname",
      "dbname",
      "--dbuser",
      "dbuser",
      "--dbpass",
      "dbpass",
      "--listen-port",
      "9002",
      "--listen-ip",
      "127.0.0.1"
    ])

    generated_config = File.read!(tmp_path() <> "generated_config.exs")
    assert generated_config =~ "host: \"fuckgab.com\""
    assert generated_config =~ "name: \"Fuck Gab!\""
    assert generated_config =~ "content: \"Test description\""
    assert generated_config =~ "hostname: \"dbhost\""
    assert generated_config =~ "database: \"dbname\""
    assert generated_config =~ "username: \"dbuser\""
    assert generated_config =~ "password: \"dbpass\""
    assert generated_config =~ "http: [ip: {127, 0, 0, 1}, port: 9002]"
    assert File.read!(tmp_path() <> "setup.psql") == generated_setup_psql()

    # Let's also make sure the shell_error is returned when there's no --force option and the generated files already exist.
    Mix.Tasks.FuckGab.Config.run([
      "gen",
      "--output",
      tmp_path() <> "generated_config.exs",
      "--output-psql",
      tmp_path() <> "setup.psql"
    ])

    assert_received {:mix_shell, :error, [message]}

    assert message ==
             "The task would have overwritten the following files:\n- #{tmp_path()}generated_config.exs\n- #{
               tmp_path()
             }setup.psql\nRerun with `--force` to overwrite them."
  end

  defp generated_setup_psql do
    ~s(CREATE USER dbuser WITH ENCRYPTED PASSWORD 'dbpass';\nCREATE DATABASE dbname OWNER dbuser;\n\\c dbname;\n--Extensions made by ecto.migrate that need superuser access\nCREATE EXTENSION IF NOT EXISTS citext;\nCREATE EXTENSION IF NOT EXISTS pg_trgm;\nCREATE EXTENSION IF NOT EXISTS \"uuid-ossp\";\n)
  end
end

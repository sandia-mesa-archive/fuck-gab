defmodule Mix.Tasks.FuckGab.InstanceTest do
  alias FuckGab.Instance
  alias FuckGab.Repo
  use FuckGab.DataCase
  import FuckGab.Factory

  setup_all do
    Mix.shell(Mix.Shell.Process)

    on_exit(fn ->
      Mix.shell(Mix.Shell.IO)
    end)

    :ok
  end

  describe "running add" do
    test "add an instance" do
      Mix.Tasks.FuckGab.Instance.run(["add", "traboone.com"])

      assert_received {:mix_shell, :info, [message]}
      assert message =~ "created"

      instance = Instance.get_by_domain("traboone.com")
      assert instance.domain == "traboone.com"
    end

    test "with errors" do
      Mix.Tasks.FuckGab.Instance.run(["add", "duckduckgo.com"])

      assert_received {:mix_shell, :error, [message]}
      assert message =~ "is not an instance"
    end
  end

  describe "running rm" do
    test "instance is deleted" do
      _instance = insert(:instance, domain: "gab.com")

      # Prepare to answer yes
      send(self(), {:mix_shell_input, :prompt, "Y"})

      Mix.Tasks.FuckGab.Instance.run(["rm", "gab.com"])

      assert_received {:mix_shell, :prompt, [message]}
      assert message =~ "Are you sure you want to remove the listing for"

      assert_received {:mix_shell, :info, [message]}
      assert message =~ "Deleted listing for"
    end

    test "instance is not deleted" do
      instance = insert(:instance, domain: "anime.website")

      # Prepare to answer yes
      send(self(), {:mix_shell_input, :prompt, "n"})

      Mix.Tasks.FuckGab.Instance.run(["rm", "anime.website"])

      assert_received {:mix_shell, :prompt, [message]}
      assert message =~ "Are you sure you want to remove the listing for"

      instance_check = Repo.reload(instance)
      assert instance.domain == instance_check.domain
    end

    test "instance does not exist" do
      Mix.Tasks.FuckGab.Instance.run(["rm", "parler.com"])

      assert_received {:mix_shell, :error, [message]}
      assert message =~ "No listing for"
    end
  end

  describe "running pending" do
    test "pending when there's 1 instance pending approval" do
      instance = insert(:instance, domain: "kiwifarms.cc", is_approved: false)

      Mix.Tasks.FuckGab.Instance.run(["pending"])
      assert_received {:mix_shell, :info, [message]}
      assert_received {:mix_shell, :info, [message2]}

      assert message =~ "There is 1 instance pending approval:"
      assert message2 =~ "- #{Instance.get_url(instance)}"
    end

    test "pending when there's more than 1 instance pending approval" do
      instance = insert(:instance, domain: "kiwifarms.cc", is_approved: false)
      instance2 = insert(:instance, domain: "anime.website", is_approved: false)

      Mix.Tasks.FuckGab.Instance.run(["pending"])
      assert_received {:mix_shell, :info, [message]}
      assert_received {:mix_shell, :info, [message2]}
      assert_received {:mix_shell, :info, [message3]}
      assert message =~ "There are 2 instances pending approval:"
      assert message2 =~ "- #{Instance.get_url(instance)}"
      assert message3 =~ "- #{Instance.get_url(instance2)}"
    end

    test "pending when there's no instances pending approval" do
      Mix.Tasks.FuckGab.Instance.run(["pending"])
      assert_received {:mix_shell, :info, [message]}
      assert message =~ "No instances pending approval."
    end
  end

  describe "run approve" do
    test "when instance is not approved yet" do
      _instance = insert(:instance, domain: "traboone.com", is_approved: false)

      Mix.Tasks.FuckGab.Instance.run(["approve", "traboone.com"])
      assert_received {:mix_shell, :info, [message]}
      assert message =~ "Approved listing for"
    end

    test "when instance has already been approved" do
      _instance = insert(:instance, domain: "traboone.com")

      Mix.Tasks.FuckGab.Instance.run(["approve", "traboone.com"])
      assert_received {:mix_shell, :error, [message]}
      assert message =~ "Instance has already been approved."
    end

    test "when instance does not exist" do
      Mix.Tasks.FuckGab.Instance.run(["approve", "traboone.com"])
      assert_received {:mix_shell, :error, [message]}
      assert message =~ "No listing for"
    end
  end
end

defmodule Mix.Tasks.FuckGab.ScraperTest do
  alias FuckGab.Repo
  use FuckGab.DataCase
  import FuckGab.Factory

  setup_all do
    Mix.shell(Mix.Shell.Process)

    on_exit(fn ->
      Mix.shell(Mix.Shell.IO)
    end)

    :ok
  end

  describe "running scrape_all" do
    test "scrape_all" do
      instance = insert(:instance, domain: "traboone.com")
      instance2 = insert(:instance, domain: "gameliberty.club")

      Mix.Tasks.FuckGab.Scraper.run(["scrape_all"])

      instance = Repo.reload(instance)
      instance2 = Repo.reload(instance2)

      assert instance.data == json_file("test/fixtures/traboone-instance.json")
      assert instance2.data == json_file("test/fixtures/game-liberty-club-instance.json")
    end
  end

  # Function to read a JSON file
  defp json_file(filename) do
    filename
    |> File.read!()
    |> Jason.decode!()
  end
end

# Modified from Pleroma, test/support/helpers.ex
# Copyright © 2017-2020 Pleroma Authors <https://pleroma.social/>
# SPDX-License-Identifier: AGPL-3.0-only

defmodule FuckGab.Tests.Helpers do
  @moduledoc """
  Helpers for use in tests.
  """
  alias FuckGab.Config
  alias Plug.Crypto.KeyGenerator
  alias Plug.Crypto.MessageEncryptor

  defmacro clear_config(config_path) do
    quote do
      clear_config(unquote(config_path)) do
      end
    end
  end

  defmacro clear_config(config_path, do: yield) do
    quote do
      initial_setting = Config.fetch(unquote(config_path))
      unquote(yield)

      on_exit(fn ->
        case initial_setting do
          :error ->
            Config.delete(unquote(config_path))

          {:ok, value} ->
            Config.put(unquote(config_path), value)
        end
      end)

      :ok
    end
  end

  defmacro clear_config(config_path, temp_setting) do
    quote do
      clear_config(unquote(config_path)) do
        Config.put(unquote(config_path), unquote(temp_setting))
      end
    end
  end

  defmacro __using__(_opts) do
    quote do
      import FuckGab.Tests.Helpers,
        only: [
          clear_config: 1,
          clear_config: 2
        ]

      # CAPTCHA unit test helpers
      def decrypt_answer_data(token, answer_data) do
        {secret, sign_secret} = secret_pair(token)

        with {:ok, data} <- MessageEncryptor.decrypt(answer_data, secret, sign_secret),
             %{at: at, answer_data: answer_md5} <- :erlang.binary_to_term(data) do
          {:ok, %{at: at, answer_data: answer_md5}}
        end
      end

      defp secret_pair(token) do
        secret_key_base = FuckGab.Config.get!([FuckGabWeb.Endpoint, :secret_key_base])
        secret = KeyGenerator.generate(secret_key_base, token <> "_encrypt")
        sign_secret = KeyGenerator.generate(secret_key_base, token <> "_sign")

        {secret, sign_secret}
      end
    end
  end
end

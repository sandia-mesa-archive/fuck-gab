defmodule FuckGab.HTTPoisonMock do
  def request(:get, "https://traboone.com/api/v1/instance") do
    {:ok,
     %HTTPoison.Response{
       body: File.read!("test/fixtures/traboone-instance.json"),
       status_code: 200
     }}
  end

  def request(:get, "https://skippers-bin.com/api/v1/instance") do
    {:ok,
     %HTTPoison.Response{
       body: File.read!("test/fixtures/skippers-bin-instance"),
       status_code: 404
     }}
  end

  def request(:post, "https://skippers-bin.com/api/meta") do
    {:ok,
     %HTTPoison.Response{
       body: File.read!("test/fixtures/skippers-bin-meta.json"),
       status_code: 200
     }}
  end

  def request(:get, "https://duckduckgo.com/api/v1/instance") do
    {:ok,
     %HTTPoison.Response{
       body: File.read!("test/fixtures/duck-duck-go-instance.html"),
       status_code: 200
     }}
  end

  def request(:post, "https://duckduckgo.com/api/meta") do
    {:ok,
     %HTTPoison.Response{
       body: File.read!("test/fixtures/duck-duck-go-meta.html"),
       status_code: 405
     }}
  end

  def request(:get, "https://gameliberty.club/api/v1/instance") do
    {:ok,
     %HTTPoison.Response{
       body: File.read!("test/fixtures/game-liberty-club-instance.json"),
       status_code: 200
     }}
  end
end
